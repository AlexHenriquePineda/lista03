package ED_lista_03_ex03e04;

public class Fila {
	
	private Livro dados[];
	private int tamanho = 0;

	public Fila(int exem) {

		dados = new Livro[exem];
	}
	
	public void adicionar(Livro e) {
		if (tamanho < dados.length) {
			dados[tamanho] = e;
			tamanho++;
		} else {
			System.out.println("Fila cheia");
		}
		
		
	}
	
	public void remover() {
		if (tamanho != 0) {
			for (int i = 0; i < dados.length; i++) {
				dados[i] = dados[i + 1];
			}
			tamanho--;
		} else {
			System.out.println("Fila vazia");
		}
	}
	
	public String toString() {
		StringBuilder string = new StringBuilder();

		for (int i = 0; i < tamanho; i++) {
			string.append("Titulo : " + dados[i].getTitulo() + ", Quantidade: " + dados[i].getExemplares() + "\n");
		}
		return string.toString();	
	}

}
