package ED_lista_03_ex03e04;

import javax.swing.JOptionPane;

public class TesteDasClasses {

	public static void main(String[] args) {
		String title;
		int qtdLivros;
		int qtd = 10;
		Pilha dadosPilha = new Pilha(qtd);
		Fila dadosFila = new Fila(qtd);

		int opc = 0;
		while (opc != 99) {

			opc = Integer.parseInt(JOptionPane.showInputDialog("1 - Adicionar livro na pilha \n"
							+ "2 - Retirar livro da pilha \n" + "3 - Mostrar pilha; \n4 - Adicionar livro na fila \n"
							+ "5 - Retirar livro da fila \n" + "6 - Mostrar na fila;\n" + "99 - Sair."));

			switch (opc) {
			case 1:
				title = JOptionPane.showInputDialog("Informe o T�tulo");
				qtdLivros = Integer.parseInt(JOptionPane.showInputDialog("Digite a quantidade de Livros: "));
				dadosPilha.adicionar(new Livro(title, qtdLivros));
				break;
			case 2:
				System.out.println("Elemento removido");
				dadosPilha.remover();
				break;
			case 3:
				System.out.println(dadosPilha);
				break;
			case 4:
				title = JOptionPane.showInputDialog("Informe o T�tulo do livro");
				qtdLivros = Integer.parseInt(JOptionPane.showInputDialog("Digite a quantidade de Livros: "));
				dadosFila.adicionar(new Livro(title, qtdLivros));
				break;
			case 5:
				System.out.println("Livro removido");
				dadosFila.remover();
				break;
			case 6:
				System.out.println(dadosFila);
				break;
			case 99:
				JOptionPane.showMessageDialog(null, "Finalizando aplica��o.");
				break;
			default:
				JOptionPane.showMessageDialog(null, "Op��o inv�lida");
			}
		}
	}

	}


