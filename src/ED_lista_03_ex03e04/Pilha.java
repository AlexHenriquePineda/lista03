package ED_lista_03_ex03e04;

public class Pilha {
	private Livro dados[];
	private int tamanho = 0;

	public Pilha(int exem) {

		dados = new Livro[exem];
	}

	public void adicionar(Livro e) {
		if (tamanho < dados.length) {
			dados[tamanho] = e;
			tamanho++;
		}else {
			System.out.println("Pilha cheia");
		}
	}
	
	public void remover() {
		if(tamanho != 0) {
			tamanho--;
		}else {
			System.out.println("Pilha vazia");
		}
	}
	
	public String percorrer() {
		StringBuilder string = new StringBuilder();
		for (int i=0; i<tamanho;i++) {
			string.append("Titulo : "+ dados[i].getTitulo() + ", Quantidade: " + dados[i].getExemplares() + "\n");
		}
		return string.toString();
	}

}
