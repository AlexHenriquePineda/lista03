package ED_lista_03;

public class ex02 {
	private int pilha[];
	private int tamanho;
	
	public ex02() {
		pilha = new int[10];
		tamanho = 0;
	}
	
	public void adicionar(int num) {
		
		if (tamanho < pilha.length) {
			for (int i =tamanho; i>0; i--) {
				pilha[i] = pilha[i-1];
			}
			pilha[0] = num;
			tamanho = tamanho + 1;
			if (tamanho != 0) {
				System.out.println("O primeiro indice � 0");
				System.out.println("O primeiro elemento da pilha �: " + pilha[0]);
				System.out.println("O �ltimo indice �: " + (tamanho-1));
				System.out.println("O ultimo elemento �: " + pilha[tamanho-1]);
			}
		} else {
			System.out.println("A pilha est� lotada");
		}
	}
	
	public int remove() {
		int x = 0;
		if (tamanho != 0) {
			x = pilha[0];
			for (int i=0; i<tamanho; i++) {
				pilha[i] = pilha[i+1];
			}
			tamanho--;
			if (tamanho != 0) {
				System.out.println("O primeiro indice � 0");
				System.out.println("O primeiro elemento da pilha �: " + pilha[0]);
				System.out.println("O �ltimo indice �: " + (tamanho-1));
				System.out.println("O ultimo elemento �: " + pilha[tamanho-1]);
			}
		} else {
			System.out.println("Pilha vazia");
		}
		return x;
	}	

}
