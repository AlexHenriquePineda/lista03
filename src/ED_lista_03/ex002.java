package ED_lista_03;

public class ex002 {
	private int fila01[];
	private int tamanho;

	public ex002() {
		fila01 = new int[10];
		tamanho = 0;
	}

	public void adicionar(int num) {

		if (tamanho < fila01.length) {
			fila01[tamanho] = num;
			tamanho++;
			if (tamanho != 0) {
				System.out.println("Primeiro indice 0"); 
				System.out.println("Primeiro elemento" + fila01[0]);
				System.out.println("Ultimo indice" + (tamanho - 1));
				System.out.println("Ultimo elemento " + fila01[tamanho - 1]);
			}
		} else {
			System.out.println("Lista lotada");
		}
	}

	public int remover() {
		int y = 0;
		if (tamanho != 0) {
			y = fila01[0];
			for (int i = 0; i < tamanho; i++) {
				fila01[i] = fila01[i + 1];
			}
			tamanho--;
			if (tamanho != 0) {
				System.out.println("Primeiro indice 0"); 
				System.out.println("Primeiro elemento" + fila01[0]);
				System.out.println("Ultimo indice" + (tamanho - 1));
				System.out.println("Ultimo elemento " + fila01[tamanho - 1]);
			}
		} else {
			System.out.println("Lista vazia");
		}
		return y;
	}

}
